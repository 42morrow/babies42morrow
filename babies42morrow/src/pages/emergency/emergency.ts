import { Component } from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-emergency',
  templateUrl: 'emergency.html'
})
export class EmergencyPage {

  public emergency = {};
  public selectOptions = {};
  public translation = {};
  constructor(public navCtrl: NavController,public http: Http) {
    this.emergency = {'title':'','description':''};
    this.selectOptions = {
      title: 'Select a language',
      alert: false
    };
    this.translation = {'language':'german','input':'', 'output':''};
  }
  
  getAdvice(){
    this.http.get(this.buildAdviceURL()).map(res => {
        res.json()
        return res.json();
      }).subscribe(data => { 
        let advice: any = document.getElementById('advice');
        let answerContainer = document.createElement('div');
        answerContainer.setAttribute('class', 'advice-container');
        answerContainer.innerHTML = data.output.text[0];   
        advice.appendChild(answerContainer);
        this.emergency['title'] = '';
        this.emergency['description'] = '';
    });
  }

  buildAdviceURL(){
    let url = 'http://42babies.eu-gb.mybluemix.net/getconversationanswer?message="'.concat(this.emergency['title']);
    url = url.concat('"');
    return url;
  }

  translate(){
    this.http.get(this.buildTranslationURL()).map(res => {
        res.json()
        return res.json();
      }).subscribe(data => {
        this.translation['output'] = data;  
    });
  }

  buildTranslationURL(){
    let url = 'http://42babies.eu-gb.mybluemix.net/translate-en-';
    switch(this.translation['language']){
      case 'german':
        url = url.concat('ge');
        break;
      case 'french':
        url = url.concat('fr');
        break;
    }
    url = url.concat('?message=%22');
    url = url.concat(this.translation['input'].replace(" ","%20"));
    url = url.concat('%22')
    return url;
  }
}
