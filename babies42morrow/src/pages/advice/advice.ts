import { Component } from '@angular/core';
import {Http} from '@angular/http';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-advice',
  templateUrl: 'advice.html'
})
export class AdvicePage {

  public age = {};
  public advice = [];
  public url = "";
  constructor(public navCtrl: NavController, public http: Http) {
    this.url = "http://42babies.eu-gb.mybluemix.net/getBaby?id=34a8592f13e4c9e632c8be7ce73bf19b";
    this.getAdvice(this.url);
    this.age = {"ageSelected":{'lower': 0, 'upper':1, 'previous_upper':1},"label":["New born",
                                          "4 months",
                                          "6 months",
                                          "8 months",
                                          "10 months",
                                          "12 months",
                                          "18 months",
                                          "24 months",
                                          "36 months"],
                "ageLabelPosition":[0,10,20,30,40,50,60,70,80]};
  }

  getAdvice(url){
    this.http.get(url).map(res => {
        res.json();
        return res.json();
      }).subscribe(data => {
        this.advice = data['advices'];  
    });
  }
}
