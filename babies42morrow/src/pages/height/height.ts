import { Component } from "@angular/core";
import { ViewController } from "ionic-angular";

@Component({
  template: `
    <img id="height-img" src="../assets/img/length-for-age.PNG">
  `
})

export class HeightPopover{

  constructor(public viewCtrl: ViewController){
  }
}