import { PopoverController } from 'ionic-angular';
import { Component } from '@angular/core';
import {Http} from '@angular/http';
import { HeightPopover } from '../height/height';

import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage {

  private popover: any;
  private userUrl = '';
  public userData = {};
  
  constructor(public navCtrl: NavController, public popoverCtrl: PopoverController, public http: Http) {
    this.popover = this.popoverCtrl.create(HeightPopover);
    this.userUrl = "http://42babies.eu-gb.mybluemix.net/getBaby?id=752fc41275b27611e5fd01c576a4d3a5";
    this.getUserInfo();
  }

  getUserInfo(){
    this.http.get(this.userUrl).map(res => {
        res.json();
        return res.json();
      }).subscribe(data => {
        this.userData = data;  
    });
    /*this.userData = '{"_id":"752fc41275b27611e5fd01c576a4d3a5","_rev":"2-78db3f631dea446f203fc54d3c73d921","firstname":"John","surname":"Snow","birthdate":"2016-10-01T00:00:00Z","insurance":{"name":"AXA","expirationdate":"2017-10-01T00:00:00Z"},"intolerances":[{"type":"nuts"},{"type":"lactose"},{"type":"gluten"},{"type":"bee"},{"type":"pollen"}],"measurements":[{"weight":{"value":4.1,"measure":"kg"},"height":{"value":52,"measure":"cm"},"temperature":{"value":36.6,"measure":"C"},"stools":{"description":"regular","viscosity":"soft"},"sleep":{"hoursperday":13},"timestamp":"2016-10-14T00:00:00Z"}],"medications":[{"name":"Vitamin C","dose":"1 pill","schedule":"1 per day (morning)"}],"address":{"street":"Steinstrasse","zipcode":"8003","building":"21"},"eventhistory":[{"type":"doctor consultation","timestamp":"2016-10-10T00:00:00Z"},{"type":"vaccination","timestamp":"2016-10-13T00:00:00Z","details":"against measles"},{"type":"vaccination","timestamp":"2016-10-10T00:00:00Z","details":"against hepatitis B"},{"type":"first steps","timestamp":"2016-08-09T00:00:00Z"}]}';*/
  }

  heightPopover() {
    this.popover.present();
  }
}
