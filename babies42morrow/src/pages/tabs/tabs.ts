import { Component } from '@angular/core';

import { ProfilePage } from '../profile/profile';
import { EmergencyPage } from '../emergency/emergency';
import { AdvicePage } from '../advice/advice';
import { CommunityPage } from '../community/community';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  tab1Root: any = ProfilePage;
  tab2Root: any = EmergencyPage;
  tab3Root: any = AdvicePage;
  tab4Root: any = CommunityPage;

  constructor() {

  }
}
