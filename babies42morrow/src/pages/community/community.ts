import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-community',
  templateUrl: 'community.html'
})
export class CommunityPage {

  public activities = {};
  public community = '';
  constructor(public navCtrl: NavController) {
    this.community = 'Russian';
    this.activities = [{"date":"20 Oct. 2016", "title":"Trolley walk in the city", "description":"The Russian association organizes its 11th gathering at the ..."},
                       {"date":"10 Nov. 2016", "title":"Kathie's Birthday", "description":"The Russian association organizes its 11th gathering at the ..."},
                       {"date":"12 Dec. 2016", "title":"Christmas celebration", "description":"The Russian association organizes its 11th gathering at the ..."}]
  }

}
