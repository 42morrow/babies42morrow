import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { ProfilePage } from '../pages/profile/profile';
import { EmergencyPage } from '../pages/emergency/emergency';
import { AdvicePage } from '../pages/advice/advice';
import { CommunityPage } from '../pages/community/community';
import { HeightPopover } from '../pages/height/height';
import { TabsPage } from '../pages/tabs/tabs';

@NgModule({
  declarations: [
    MyApp,
    ProfilePage,
    EmergencyPage,
    AdvicePage,
    CommunityPage,
    HeightPopover,
    TabsPage
  ],
  imports: [
    IonicModule.forRoot(MyApp,{
    tabsPlacement:'top'},{})
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ProfilePage,
    EmergencyPage,
    AdvicePage,
    CommunityPage,
    HeightPopover,
    TabsPage
  ],
  providers: []
})
export class AppModule {}
